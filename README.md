Требуемые библиотеки для запуска:  

* numpy
* matplotlib

Запуск демо-скриптов:  
k-means:  
`python3 -m da_lab.demo.kmeans`  
построение kd-дерева:  
`python3 -m da_lab.demo.kd_tree_build`

Запуск тестов алгоритмов на kd-дереве:  
`python3 -m unittest -s da_lab`
