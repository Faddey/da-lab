import numpy as np
import csv


def load(path):
    lines = list(csv.reader(open(path)))
    features = lines[0][:-1]
    lines = lines[1:]
    data = np.array([l[:-1] for l in lines], dtype=np.float)
    labels = [l[-1] for l in lines]
    classes = sorted(set(labels))
    labels = np.array(list(map(classes.index, labels)))
    return DataSet(data, features, classes, labels)


class DataSet(object):

    def __init__(self, data, features, class_names, labels):
        self.data = data
        self.features = features
        self.class_names = class_names
        self.labels = labels


