import os
from da_lab import data_io


def get_iris_dataset():
    path = __file__
    for i in range(3):
        path = os.path.dirname(path)
    path = os.path.join(path, 'iris.csv')
    return data_io.load(path)
