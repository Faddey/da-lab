import numpy as np
from matplotlib import pyplot as plt
from da_lab import kd_tree, visualizing
from ._common import get_iris_dataset


def main():

    ds = get_iris_dataset()

    data = np.matmul(ds.data, visualizing.generate_projections(4)[26])
    tree = kd_tree.KDTree(data)
    tree.build()

    visualizing.draw_kdtree(tree, plt.gca(), 4)

    plt.show()

if __name__ == '__main__':
    main()
