import numpy as np
from matplotlib import pyplot as plt
from da_lab import k_means, visualizing
from ._common import get_iris_dataset


def main():

    ds = get_iris_dataset()

    km = k_means.KMeans(ds.data)
    km.start(3, lambda x, c: np.sum((x-c)**2, axis=-1))

    projections = visualizing.generate_projections(4)
    projections = [projections[i] for i in [0, 12, 29, 33]]

    fig, axes = plt.subplots(2, len(projections), squeeze=False)

    axes_tgt = axes[1, :]
    axes_alg = axes[0, :]
    visualizing.draw_clusters(
        data=ds.data,
        projections=projections,
        assignment=ds.labels,
        axes=axes_tgt,
        colors=['r', 'g', 'b']
    )

    step_i = 0

    while True:
        step_i += 1
        print('Step:', step_i)
        plt.ioff()
        for ax in axes_alg.flatten():
            ax.clear()
        visualizing.draw_clusters(
            data=ds.data,
            projections=projections,
            assignment=km.assignment,
            axes=axes_alg,
            colors=['r', 'g', 'b']
        )
        plt.ion()
        plt.draw()
        plt.show()
        plt.pause(2)
        done = km.step()
        if done:
            break
    print('Done. Close plot to exit.')
    plt.ioff()
    plt.show()


if __name__ == '__main__':
    main()
