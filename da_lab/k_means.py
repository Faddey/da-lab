import numpy as np
import random


class KMeans(object):

    def __init__(self, data):
        """
        :type data: numpy.ndarray
        """
        self.data = data
        self.distance = None
        self.k = None
        self.centers = None
        self.fitting_done = False
        self._assignment = None

    def start(self, k, distance):
        self.k = k
        self.distance = distance
        sample_idx = random.sample(range(self.data.shape[0]), k)
        self.centers = self.data[tuple(sample_idx), ]
        self.fitting_done = False
        self._assignment = None

    def calculate_assignment(self):
        data = np.expand_dims(self.data, axis=1)
        centers = np.expand_dims(self.centers, axis=0)
        dists = self.distance(data, centers)
        return np.argmin(dists, axis=1)

    @property
    def assignment(self):
        if self._assignment is None:
            self._assignment = self.calculate_assignment()
        return self._assignment

    def step(self):
        asmt = self.assignment
        self._assignment = None
        new_centers = np.empty((self.k,)+self.data.shape[1:])
        for cls in range(self.k):
            cluster = self.data[(asmt == cls, )]
            if cluster.size > 0:
                center = np.sum(cluster, axis=0) / cluster.shape[0]
            else:
                center = np.zeros(cluster.shape[1:], dtype=cluster.dtype)
            new_centers[cls] = center
        old_centers = self.centers
        self.centers = new_centers
        return np.allclose(new_centers, old_centers)

    def build_clusters(self, k, distance):
        self.start(k, distance)
        while self.step():
            pass
