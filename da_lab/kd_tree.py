import numpy as np


class Item(object):

    __slots__ = ('point', 'index')

    def __init__(self, point, index):
        self.point = point
        self.index = index


class Node(object):

    __slots__ = ('item', 'left', 'right')

    def __init__(self, item=None, left=None, right=None):
        self.item = item
        self.left = left
        self.right = right


class KDTree(object):

    def __init__(self, data):
        self.data = data
        self.size, self.ndim = data.shape
        self.root = None

    def build(self):
        indices = list(range(self.size))
        self.root = self._build_recursive(indices, 0)

    def _build_recursive(self, indices, level):
        this_size = len(indices)
        if this_size == 0:
            return
        dim = level % self.ndim

        indices.sort(key=lambda i: self.data[i, dim])

        median_pos = int(this_size / 2)
        median_idx = indices[median_pos]
        median_pt = self.data[median_idx]

        left_indices = indices[:median_pos]
        right_indices = indices[median_pos+1:]

        return Node(
            Item(median_pt, median_idx),
            self._build_recursive(left_indices, level+1),
            self._build_recursive(right_indices, level+1),
        )

    def query_range(self, pt1, pt2):
        result = []
        pt1, pt2 = np.array([pt1, pt2])
        self._query_range_recursive(self.root, pt1, pt2, result)
        return np.array(result, dtype=np.int)

    def query_radius(self, pt, r):
        range_idx = self.query_range(pt-r, pt+r)
        pts = self.data[range_idx]
        return range_idx[self.distances(pts, pt) <= r]

    def find_n_nearest(self, pt, n):
        if n > self.size:
            raise ValueError
        last_n = best = self.root
        best_d = self.distances(best.item.point, pt)
        dim = 0
        while True:
            if pt[dim] > last_n.item.point[dim]:
                next_n = last_n.left
            else:
                next_n = last_n.right
            if next_n is None:
                break
            dim = (dim+1) % self.ndim
            next_d = self.distances(next_n.item.point, pt)
            if next_d < best_d:
                best = next_n
                best_d = next_d
            last_n = next_n
        base_d = d = self.distances(best.item.point, pt)
        i = 0
        while True:
            result = self.query_radius(pt, d)
            i += 1
            # print('i={} sz={}'.format(i, result.size))
            if result.size >= n:
                break
            d += base_d
        if result.size > n:
            dists = self.distances(self.data[result], pt)
            result = result[np.argpartition(dists, n)[:n]]
        return result

    def _query_range_recursive(self, node, pt1, pt2, results):
        if node is None:
            return
        p = node.item.point
        if np.all(pt1 > p):
            self._query_range_recursive(node.right, pt1, pt2, results)
        elif np.all(pt2 < p):
            self._query_range_recursive(node.left, pt1, pt2, results)
        else:
            if np.all(pt1 <= p) and np.all(p <= pt2):
                self._add_result(node, results)
            self._query_range_recursive(node.right, pt1, pt2, results)
            self._query_range_recursive(node.left, pt1, pt2, results)

    def _add_result(self, node, results):
        results.append(node.item.index)

    def distances(self, p1, p2):
        if p1.ndim > 1 and p2.ndim == 1:
            p2 = np.expand_dims(p2, 0)
        return np.sum(np.square(p1-p2), -1)


class NearestNeighborClassifier(object):

    def __init__(self, data, labels, n=10):
        self.tree = KDTree(data)
        self.labels = labels
        self.n = n

    def build(self):
        self.tree.build()

    def classify(self, pt):
        nearest_idx = self.tree.find_n_nearest(pt, self.n)
        nearest_labels = self.labels[nearest_idx]
        matched_labels = set(nearest_labels)
        if len(matched_labels) == 1:
            return matched_labels.pop()

        nearest_points = self.tree.data[nearest_idx]
        nearest_dists = self.tree.distances(nearest_points, pt)
        vote_weights = 1 / (nearest_dists + 0.00001)
        votes = {}
        for label in matched_labels:
            votes[label] = np.sum(vote_weights[nearest_labels == label])
        return max(votes.keys(), key=votes.__getitem__)
