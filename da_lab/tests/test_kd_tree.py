import unittest
import numpy as np
from da_lab import kd_tree


class TestKdTree(unittest.TestCase):

    SIZE = 1000

    def test_query_range(self):
        for i in range(10):
            data = np.random.standard_normal((self.SIZE, 4))

            # these parameters give such range that contains about 20% of data
            pt1 = -np.abs(3 * np.random.standard_normal(4))
            pt2 = pt1 + 3

            kdt = kd_tree.KDTree(data)
            kdt.build()

            result = set(kdt.query_range(pt1, pt2))

            expected_mask = np.all(data >= pt1, 1) & np.all(data <= pt2, 1)
            expected = set(np.arange(self.SIZE)[expected_mask])
            self.assertSetEqual(result, expected)

    def test_query_radius(self):
        for i in range(10):
            data = np.random.standard_normal((self.SIZE, 4))
            # these parameters give such sphere that contains about 10% of data
            pt = np.random.standard_normal(4)
            r = 2

            kdt = kd_tree.KDTree(data)
            kdt.build()

            result = set(kdt.query_radius(pt, r))

            expected_mask = kdt.distances(data, pt) <= r
            expected = set(np.arange(self.SIZE)[expected_mask])
            self.assertSetEqual(result, expected)

    def test_find_n_nearest(self):
        for i in range(5):
            data = np.random.standard_normal((self.SIZE, 4))
            pt = np.random.standard_normal(4)
            n = 10

            kdt = kd_tree.KDTree(data)
            kdt.build()

            result = set(kdt.find_n_nearest(pt, n))

            dists = kdt.distances(data, pt)
            n_nearest = set(np.argpartition(dists, n)[:n])
            self.assertSetEqual(result, n_nearest)
