import numpy as np
from itertools import permutations


def draw_clusters(data, projections, assignment, axes, colors):
    if isinstance(axes, np.ndarray):
        axes = axes.flatten()

    for i, proj in enumerate(projections):
        view = np.matmul(data, proj)
        for cls, clr in enumerate(colors):
            cluster = view[(assignment == cls, )]
            f1, f2 = cluster[:, 0], cluster[:, 1]
            axes[i].scatter(f1, f2, color=clr)


def generate_projections(ndim):
    def mkarr_06_08(i0_08, i0_06, i1_08, i1_06):
        arr = np.zeros((ndim, 2))
        arr[i0_08, 0] = 0.8
        arr[i0_06, 0] = 0.6
        arr[i1_08, 1] = 0.8
        arr[i1_06, 1] = 0.6
        return arr

    def mkarr_1_1(i0, i1):
        arr = np.zeros((ndim, 2))
        arr[i0, 0] = 1
        arr[i1, 1] = 1
        return arr

    return [
        mkarr_06_08(i0_08, i0_06, i1_08, i1_06)
        for (i0_08, i0_06, i1_08, i1_06) in
        permutations(range(ndim), ndim)
    ] + [
        mkarr_1_1(i0, i1)
        for i0, i1 in permutations(range(ndim), 2)
    ]


def draw_kdtree(kdtree, axis, max_levels):
    if kdtree.ndim != 2:
        raise ValueError("Only 2D trees are supported")
    draw_clusters(
        data=kdtree.data,
        projections=[np.eye(2)],
        assignment=np.zeros([kdtree.size]),
        axes=[axis],
        colors=['b'],
    )
    _draw_2dtree_dividers(
        axis, kdtree.root,
        np.array([
            axis.get_xlim(),
            axis.get_ylim(),
        ]),
        max_levels, 0
    )


def _draw_2dtree_dividers(axis, node, rect, levels, dim):
    if levels == 0 or node is None:
        return
    left_rect, right_rect = np.copy(rect), np.copy(rect)
    p = node.item.point
    z = p[dim]
    line = np.copy(rect)
    line[dim, :] = z
    axis.plot(*line, 'r-', clip_on=False)
    axis.scatter([p[0]], [p[1]], color='g')
    left_rect[dim, 1] = z
    right_rect[dim, 0] = z
    _draw_2dtree_dividers(axis, node.right, right_rect, levels-1, (dim+1) % 2)
    _draw_2dtree_dividers(axis, node.left, left_rect, levels-1, (dim+1) % 2)
