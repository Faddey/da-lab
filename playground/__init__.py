
try:
    from fflib.playground.plugins import hot_reload
except ImportError:
    pass
else:
    hot_reload.setup(['da_lab'])
