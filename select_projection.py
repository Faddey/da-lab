from matplotlib import pyplot as plt
from da_lab import data_io, visualizing


def main():
    ds = data_io.load('iris.csv')

    projections = visualizing.generate_projections(4)
    assert len(projections) == 36  # P(4, 4) + P(4, 2) = 24 + 12

    fig, axes = plt.subplots(6, 6, squeeze=False)
    visualizing.draw_clusters(
        data=ds.data,
        projections=projections,
        assignment=ds.labels,
        axes=axes,
        colors=['r', 'g', 'b']
    )
    plt.show()


if __name__ == '__main__':
    main()
